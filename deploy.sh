#!/bin/bash

WORKING_DIR=$(dirname "$0")
env=$1
if [ -z "$env" ]; then 
  echo "env argument must be supplied";
  exit 1 
fi

kubectl apply -f $WORKING_DIR/kapp/kapp-sa.yaml
kubectl apply -f $WORKING_DIR/kapp/base-config.yaml

ytt -f $WORKING_DIR/kapp/common-config-template.yaml -f $WORKING_DIR/ytt/ytt-defaults.yaml -v env=$env | kubectl apply -f-
